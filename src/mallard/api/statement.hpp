#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

namespace mallard {
namespace api {

struct Statement {
    std::vector<std::string> literals;
    std::vector<std::shared_ptr<Statement>> productions;
    std::string type;

    enum {
        VARIABLE = 0,
        INT_LITERAL,
        FLOAT_LITERAL,
        DOUBLE_LITERAL,
        STRING_LITERAL,
        BINARY_EXPRESSION,
        ASSIGNMENT
    } id;
};

}
}
