#include "statement.hpp"

#include <ostream>

namespace mallard {
namespace api {

std::ostream& operator <<(std::ostream& os, const Statement& statement);

}
}