#include "mallard/api/statement_debug.hpp"

using std::ostream;

namespace mallard {
namespace api {

ostream& operator <<(ostream& os, const Statement& statement) {
    os << "Statement(id=" << statement.id << ", ";

    switch(statement.id) {
        case Statement::VARIABLE:
            os << "variable=" << statement.literals[0];
            break;
        case Statement::INT_LITERAL:
        case Statement::FLOAT_LITERAL:
        case Statement::DOUBLE_LITERAL:
        case Statement::STRING_LITERAL:
            os << "literal=" << statement.literals[0];
            break;
        case Statement::BINARY_EXPRESSION:
            os << "left=" << *statement.productions[0]
                << ", symbol=" << statement.literals[0]
                << ", right=" << *statement.productions[1];
            break;
        case Statement::ASSIGNMENT:
            os << "variable=" << statement.literals[0]
                << ", expression=" << *statement.productions[0];
            break;
    }

    os << ")";
    return os;
}

}
}
