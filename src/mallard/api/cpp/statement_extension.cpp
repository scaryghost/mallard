#include "mallard/api/statement_extension.hpp"

using namespace std;

namespace mallard {
namespace api {

static bool operator ==(const vector<shared_ptr<Statement>>& left, const vector<shared_ptr<Statement>>& right) {
    if (left.size() != right.size()) {
        return false;
    }

    auto l_it = left.begin();
    auto r_it = right.begin();

    while(l_it != left.end()) {
        if (!((**l_it) == (**r_it))) {
            return false;
        }

        l_it++;
        r_it++;
    }
    
    return true;
}

bool operator ==(const Statement& left, const Statement& right) {
    return left.id == right.id && left.type == right.type && 
        left.literals == right.literals && 
        left.productions == right.productions;
}

}
}
