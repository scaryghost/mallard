#include "statement.hpp"

#include <ostream>

namespace mallard {
namespace api {

bool operator ==(const Statement& left, const Statement& right);

}
}
