#include "tokenizer.hpp"

#include "state.hpp"

#include "mallard/parser/exception.hpp"

#include <cctype>
#include <unordered_set>

using namespace std;

namespace mallard {
namespace parser {

const unordered_set<string> KEYWORDS = {
    "val", "return"
};

const vector<State> symbol_states = {
    {
        .transitions = { 
            {'(', 1}, {')', 1}, {'{', 1}, {'}', 1}, {'[', 1}, {']', 1}, {'?', 1}, {':', 1},
            {'/', 2}, {'%', 2}, {'=', 2},
            {'+', 3}, 
            {'-', 4}, 
            {'*', 5}, 
            {'<', 6}, 
            {'>', 7}, 
            {'~', 2}, 
            {'^', 8}, 
            {'|', 9}, 
            {'&', 10}, 
            {'.', 11}, 
            {'!', 2}, 
        },
        .is_accepting = false
    },
    {
        .is_accepting = true
    },
    {
        .transitions = { {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'+', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'-', 1}, {'=', 1}, {'>', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'*', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'<', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'>', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'^', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'|', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'&', 1}, {'=', 1} },
        .is_accepting = true
    },
    {
        .transitions = { {'.', 1} },
        .is_accepting = true
    }
};

Tokenizer::Tokenizer(istream& is) :
    is(is),
    symbol_index(0),
    read_dot(false)
{
    is.exceptions(istream::failbit | istream::eofbit | istream::badbit);
}

Token Tokenizer::next() {
    char ch = next_char();

    if (isdigit(ch)) {
        buffer << ch;
        return munch_digits();
    }

    if (ch == '\"') {
        return munch_string();
    }

    if (ch == '_' || isalpha(ch)) {
        buffer << ch;
        return munch_identifier_or_keyword();
    }

    {
        auto transitions = symbol_states.at(symbol_index).transitions;
        auto it = transitions.find(ch);

        if (it != transitions.end()) {
            buffer << ch;
            symbol_index = it->second;

            return munch_symbol();
        }
    }

    {
        stringstream msg;
        msg << "Unrecognized Mallard character: \'" << ch << "\'";

        throw ParserException(msg.str());
    }
    
}

void Tokenizer::reset_buffer() {
    buffer.str(string());
    buffer.clear();
}

uint8_t Tokenizer::next_char() {
    if (ch_buffer.empty()) {
        try {
            return is.get();
        } catch (const ios_base::failure& e) {
            if (!is.eof()) {
                throw e;
            }

            throw NoMoreTokensException("No more characters to read from the input stream");
        }
        return is.get();
    }

    auto ch = *ch_buffer.begin();

    ch_buffer.erase(ch_buffer.begin());
    return ch;
}

Token Tokenizer::munch_digits() {
    auto token_id = read_dot ? Token::DOUBLE_LITERAL : Token::INT_LITERAL;

    while(true) {
        char ch;

        try {
            ch = next_char();
        } catch (const NoMoreTokensException& e) {
            break;
        }

        if (isspace(ch)) {
            break;
        }

        if (isdigit(ch)) {
            buffer << ch;
        } else if (ch == 'f') {
            buffer << ch;
            token_id = Token::FLOAT_LITERAL;
            break;
        } else if (ch == '.') {
            if (read_dot) {
                stringstream msg;
                msg << "Decimal literal can only have at most 1 dot, received: " << buffer.rdbuf();

                throw ParserException(msg.str());
            } else {
                read_dot = true;
                return munch_digits_one_dot();
            }
        } else {
            ch_buffer.push_back(ch);
            break;
        }
    }

    Token result = {
        .value = buffer.str(),
        .type = token_id,
    };

    reset_buffer();
    read_dot = false;

    return result;
}

Token Tokenizer::munch_digits_one_dot() {
    uint8_t ch;

    try {
        ch = next_char();
    } catch (const NoMoreTokensException& e) {
        buffer << '.';

        auto digits = buffer.str();
        reset_buffer();
        read_dot = false;

        return {
            .value = digits,
            .type = Token::DOUBLE_LITERAL,
        };
    }

    if (isspace(ch)) {
        auto digits = buffer.str();
        reset_buffer();
        read_dot = false;

        return {
            .value = digits,
            .type = Token::DOUBLE_LITERAL,
        };
    }

    if (isdigit(ch)) {
        buffer << '.' << ch;
        return munch_digits();
    }

    if (ch == '.') {
        ch_buffer.push_back('.');
        ch_buffer.push_back('.');

        auto digits = buffer.str();
        reset_buffer();
        read_dot = false;

        return {
            .value = digits,
            .type = Token::INT_LITERAL
        };
    }

    if (ch == 'f') {
        buffer << ".f";

        stringstream msg;
        msg << "Float literal designator (f) must follow a digit not dot (.), : " << buffer.rdbuf();

        throw ParserException(msg.str());
    }

    ch_buffer.push_back('.');
    ch_buffer.push_back(ch);

    Token result = {
        .value = buffer.str(),
        .type = Token::INT_LITERAL
    };
    
    reset_buffer();
    read_dot = false;

    return result;
}

Token Tokenizer::munch_string() {
    while(true) {
        uint8_t ch;

        try {
            ch = next_char();
        } catch (const NoMoreTokensException& e) {
            break;
        }


        if (ch == '\\') {
            int32_t next_code;

            try {
                next_code = next_char();
            } catch (const NoMoreTokensException& e) {
                stringstream msg;
                msg << "Unfinished escapae character in string: \"" << ch << "\"";

                throw ParserException(msg.str());
            }

            switch(next_code) {
                case 34:
                    buffer << '\"';
                    break;
                case 39:
                    buffer << '\'';
                    break;
                case 92:
                    buffer << '\\';
                    break;
                case 110:
                    buffer << '\n';
                    break;
                case 114:
                    buffer << '\r';
                    break;
                case 116:
                    buffer << '\t';
                    break;
                default: {
                    stringstream msg;
                    msg << "Unrecognized escape character: \\" << (uint8_t) next_code 
                        << " (code = " << next_code << ")";

                    throw ParserException(msg.str());
                }
            }
        } else if (ch == '\"') {
            Token result = {
                .value = buffer.str(),
                .type = Token::STRING_LITERAL
            };
            
            reset_buffer();

            return result;
        } else {
            buffer << ch;
        }
    }

    {
        stringstream msg;
        msg << "Unterminated string: \"" << buffer.rdbuf();

        throw ParserException(msg.str());
    }
}

Token Tokenizer::munch_identifier_or_keyword() {
    while(true) {
        char ch;

        try {
            ch = next_char();
        } catch (const NoMoreTokensException& e) {
            break;
        }

        if (isspace(ch)) {
            break;
        }

        if (ch == '_' || isalpha(ch) || isdigit(ch)) {
            buffer << ch;
        } else {
            ch_buffer.push_back(ch);
            break;
        }
    }

    auto value = buffer.str();
    Token result = {
        .value = value,
        .type = KEYWORDS.count(value) ? Token::KEYWORD : Token::IDENTIFIER
    };
    reset_buffer();

    return result;
}

Token Tokenizer::munch_symbol() {
    while(true) {
        char ch;

        try {
            ch = next_char();
        } catch (const NoMoreTokensException& e) {
            break;
        }

        if (isspace(ch)) {
            break;
        }

        {
            auto transitions = symbol_states.at(symbol_index).transitions;
            auto it = transitions.find(ch);

            if (it != transitions.end()) {
                buffer << ch;
                symbol_index = it->second;
            } else if (symbol_states.at(symbol_index).is_accepting) {
                ch_buffer.push_back(ch);
                break;
            } else {
                stringstream msg;
                msg << "Unrecognized symbol: \"" << buffer.rdbuf() << "\"";

                throw ParserException(msg.str());
            }
        }
    }

    Token result = {
        .value = buffer.str(),
        .type = Token::SYMBOL
    };
    
    reset_buffer();
    symbol_index = 0;

    return result;
}

}
}
