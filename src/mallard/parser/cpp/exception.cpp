#include "mallard/parser/exception.hpp"

using std::runtime_error;
using std::string;

namespace mallard {
namespace parser {

ParserException::ParserException(const string& what) : runtime_error(what) {

}

NoMoreTokensException::NoMoreTokensException(const string& what) : ParserException(what) {

}

}
}
