#pragma once

#include <cstdint>
#include <string>

namespace mallard {
namespace parser {

struct Token {
    std::string value;
    enum : std::uint8_t {
        INT_LITERAL = 0,
        FLOAT_LITERAL,
        DOUBLE_LITERAL,
        STRING_LITERAL,
        IDENTIFIER,
        SYMBOL,
        KEYWORD
    } type;
};

}
}