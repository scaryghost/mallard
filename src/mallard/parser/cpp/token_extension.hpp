#pragma once

#include "token.hpp"

#include <ostream>

namespace mallard {
namespace parser {

bool operator ==(const Token& left, const Token& right);

std::ostream& operator <<(std::ostream& os, const Token& token);

}
}