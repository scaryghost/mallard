#pragma once

#include "token.hpp"

#include <cstdint>
#include <istream>
#include <sstream>
#include <string>
#include <vector>

namespace mallard {
namespace parser {

struct Tokenizer {
    Tokenizer(std::istream& is);

    Token next();

private:
    void reset_buffer();
    std::uint8_t next_char();

    Token munch_digits();
    Token munch_digits_one_dot();
    Token munch_identifier_or_keyword();
    Token munch_string();
    Token munch_symbol();

    std::ostringstream buffer;
    std::istream& is;
    std::size_t symbol_index;
    std::vector<std::uint8_t> ch_buffer;
    bool read_dot;
};

}
}