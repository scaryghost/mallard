#pragma once

#include <memory>
#include <string>

#include "token.hpp"
#include "mallard/api/statement.hpp"

namespace mallard {
namespace parser {

std::shared_ptr<api::Statement> new_assignment();
std::shared_ptr<api::Statement> build_terminal_statement(const Token& token);

std::shared_ptr<api::Statement> build_binary_expression(
    const std::shared_ptr<api::Statement>& base_expr,
    const std::string& symbol
);

void set_right_expression(
    std::shared_ptr<api::Statement>& root_expr,
    std::shared_ptr<api::Statement>& current_binary_expr,
    const std::shared_ptr<api::Statement>& right_expr
);

}
}