#include "mallard/parser/exception.hpp"
#include "mallard/parser/stream.hpp"

#include "statement_builder.hpp"
#include "token_extension.hpp"
#include "tokenizer.hpp"

#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

const unordered_set<string> BINARY_OP_SYMBOLS = {
    ".", "**", "*", "/", "%",
    "+", "-", "<<", ">>", 
    "<=>", ">", "<", ">=", "<=","==", "!=",
    "&", "^", "|", "&&", "^^", "||"
};

namespace mallard {
namespace parser {

struct InternalStatementStream : public StatementStream {
    InternalStatementStream(istream& is);
    virtual ~InternalStatementStream();

    shared_ptr<api::Statement> next() override;

private:
    Token next_token();
    shared_ptr<api::Statement> next_assignment();
    shared_ptr<api::Statement> next_expression();

    Tokenizer tokenizer;
    vector<Token> token_buffer;
    shared_ptr<api::Statement> statement_root, current_statement_node;
    bool statement_is_complete;
};

shared_ptr<StatementStream> StatementStream::from_istream(istream& is) {
    return make_shared<InternalStatementStream>(is);
}

StatementStream::~StatementStream() {

}

InternalStatementStream::InternalStatementStream(istream& is) :
    tokenizer(is),
    statement_is_complete(false)
{

}

InternalStatementStream::~InternalStatementStream() {

}

Token InternalStatementStream::next_token() {
    if (token_buffer.empty()) {
        return tokenizer.next();
    }

    auto token = *token_buffer.begin();

    token_buffer.erase(token_buffer.begin());
    return token;
}

shared_ptr<api::Statement> InternalStatementStream::next() {
    Token token;

    try {
        token = next_token();
    } catch (const NoMoreTokensException&) {
        throw runtime_error("No tokens to process");
    }

    switch(token.type) {
        case Token::KEYWORD: {
            if (token.value == "val") {
                return next_assignment();                    
            }

            stringstream msg;
            msg << "Keyword \"" << token.value << "\" not yet implemented";

            throw runtime_error(msg.str());
        }
        default: {
            token_buffer.push_back(token);
            return next_expression();
        }
    }
}

shared_ptr<api::Statement> InternalStatementStream::next_assignment() {
    auto assignment = new_assignment();

    auto token = next_token();
    switch(token.type) {
        case Token::IDENTIFIER:
            assignment->literals[0] = token.value;
            break;
        default: {
            stringstream msg;
            msg << "Identifier must follow \"val\" keyword, read " << token;

            throw runtime_error(msg.str());
        }
    }

    token = next_token();
    switch(token.type) {
        case Token::SYMBOL:
            if (token.value != "=") {
                stringstream msg;
                msg << "Assignemnt must follow identifier with \"=\", read " << token.value;

                throw runtime_error(msg.str());
            }
            break;
        default: {
            stringstream msg;
            msg << "Equals sign (=) must follow identifier, read " << token;

            throw runtime_error(msg.str());
        }
    }

    assignment->productions[0] = next_expression();
    return assignment;
}

shared_ptr<api::Statement> InternalStatementStream::next_expression() {
    while(true) {
        Token token;

        try {
            token = next_token();
        } catch (const NoMoreTokensException&) {
            if (statement_is_complete) {
                return statement_root;
            }

        }

        if (statement_root == nullptr) {
            statement_root = build_terminal_statement(token);
            statement_is_complete = true;
            continue;
        }

        if (token.type == Token::SYMBOL) {
            if (BINARY_OP_SYMBOLS.count(token.value)) {
                current_statement_node = build_binary_expression(
                    current_statement_node != nullptr ? current_statement_node : statement_root,
                    token.value
                );
                statement_is_complete = false;
                continue;
            }

            stringstream msg;
            msg << "Symbol \"" << token.value << "\" not yet supported";

            throw runtime_error(msg.str());
        }

        if (current_statement_node->id == api::Statement::BINARY_EXPRESSION) {
            set_right_expression(statement_root, current_statement_node, build_terminal_statement(token));
            statement_is_complete = true;
        }
    }
    
    throw runtime_error("Not yet implemented");
}

}
}