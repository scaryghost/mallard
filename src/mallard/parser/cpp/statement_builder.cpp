#include "statement_builder.hpp"

#include <cstdint>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>

using namespace std;

namespace mallard {
namespace parser {

const unordered_map<string, uint8_t> SYMBOL_OPERATOR_PRIORITY = {
    {".", 12},
    {"**", 11},
    {"*", 10}, {"/", 10}, {"%", 10},
    {"+", 9}, {"-", 9}, 
    {"<<", 8}, {">>", 8}, 
    {"<=>", 8}, 
    {">", 7}, {"<", 7}, {">=", 7}, {"<=", 7},
    {"==", 6}, {"!=", 6},
    {"&", 5},
    {"^", 4},
    {"|", 3},
    {"&&", 2},
    {"^^", 1},
    {"||", 0}
};

shared_ptr<api::Statement> new_assignment() {
    return shared_ptr<api::Statement>(
        new api::Statement {
            .literals = { "" },
            .productions = { nullptr },
            .id = api::Statement::ASSIGNMENT
        }
    );
}

shared_ptr<api::Statement> build_terminal_statement(const Token& token) {
    switch(token.type) {
        case Token::INT_LITERAL:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { token.value },
                    .id = api::Statement::INT_LITERAL
                }
            );
            break;
        case Token::FLOAT_LITERAL:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { token.value },
                    .id = api::Statement::FLOAT_LITERAL
                }
            );
            break;
        case Token::DOUBLE_LITERAL:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { token.value },
                    .id = api::Statement::DOUBLE_LITERAL
                }
            );
            break;
        case Token::STRING_LITERAL:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { token.value },
                    .id = api::Statement::STRING_LITERAL
                }
            );
            break;
        case Token::IDENTIFIER:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { token.value },
                    .id = api::Statement::VARIABLE
                }
            );
            break;
        default: {
            stringstream msg;

            msg << "Token type " << static_cast<int32_t>(token.type) 
                << " cannot be a terminal statement";
            throw runtime_error(msg.str());
        }
    }
}

shared_ptr<api::Statement> build_binary_expression(
    const shared_ptr<api::Statement>& base_expr, 
    const string& symbol
) {
    switch(base_expr->id) {
        case api::Statement::INT_LITERAL:
        case api::Statement::FLOAT_LITERAL:
        case api::Statement::DOUBLE_LITERAL:
        case api::Statement::STRING_LITERAL:
        case api::Statement::VARIABLE:
            return shared_ptr<api::Statement>(
                new api::Statement {
                    .literals = { symbol },
                    .productions = { base_expr, nullptr },
                    .id = api::Statement::BINARY_EXPRESSION
                }
            );
        case api::Statement::BINARY_EXPRESSION: {
            auto next_priority = SYMBOL_OPERATOR_PRIORITY.find(symbol);
            auto current_priority = SYMBOL_OPERATOR_PRIORITY.at(base_expr->literals[0]);

            if (next_priority == SYMBOL_OPERATOR_PRIORITY.end()) {
                stringstream msg;

                msg << "Unrecognized binary operator \"" << symbol << "\"";
                throw runtime_error(msg.str());
            }

            if (next_priority->second > current_priority) {
                auto higher_priority_expr = shared_ptr<api::Statement>(
                    new api::Statement {
                        .literals = { symbol },
                        .productions = { base_expr->productions[1], nullptr },
                        .id = api::Statement::BINARY_EXPRESSION
                    }
                );
                base_expr->productions[1] = higher_priority_expr;

                return higher_priority_expr;
            } else {
                auto copy = shared_ptr<api::Statement>(
                    new api::Statement {
                        .literals = base_expr->literals,
                        .productions = base_expr->productions,
                        .id = api::Statement::BINARY_EXPRESSION
                    }
                );
                base_expr->literals[0] = symbol;
                base_expr->productions[0] = copy;
                
                return base_expr;
            }
        }
    }
}

void set_right_expression(
    shared_ptr<api::Statement>& root_expr,
    shared_ptr<api::Statement>& current_binary_expr,
    const shared_ptr<api::Statement>& right_expr
) {
    current_binary_expr->productions[1] = right_expr;

    switch(root_expr->id) {
        case api::Statement::INT_LITERAL:
        case api::Statement::FLOAT_LITERAL:
        case api::Statement::DOUBLE_LITERAL:
        case api::Statement::STRING_LITERAL:
        case api::Statement::VARIABLE:
            root_expr = current_binary_expr;
            break;
        case api::Statement::BINARY_EXPRESSION: {
            auto root_priority = SYMBOL_OPERATOR_PRIORITY.at(root_expr->literals[0]);
            auto current_priority = SYMBOL_OPERATOR_PRIORITY.at(current_binary_expr->literals[0]);

            if (root_priority >= current_priority) {
                root_expr = current_binary_expr;
                return;
            }
        }
    }
}

}
}
