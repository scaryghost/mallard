#pragma once

#include <cstdint>
#include <unordered_map>
#include <vector>

namespace mallard {
namespace parser {

struct State {
    const std::unordered_map<std::uint8_t, std::size_t> transitions;
    const bool is_accepting;
};

}
}