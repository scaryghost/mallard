#include "token_extension.hpp"

using std::ostream;

namespace mallard {
namespace parser {

bool operator ==(const Token& left, const Token& right) {
    return left.type == right.type &&
        left.value == right.value;
}

ostream& operator <<(ostream& os, const Token& token) {
    os << "Token(type=";
    
    switch(token.type) {
        case Token::INT_LITERAL:
            os << "INT_LITERAL";
            break;
        case Token::FLOAT_LITERAL:
            os << "FLOAT_LITERAL";
            break;
        case Token::DOUBLE_LITERAL:
            os << "DOUBLE_LITERAL";
            break;
        case Token::STRING_LITERAL:
            os << "STRING_LITERAL";
            break;
        case Token::IDENTIFIER:
            os << "IDENTIFIER";
            break;
        case Token::SYMBOL:
            os << "SYMBOL";
            break;
    }

    os << ", value=\"" << token.value << "\")";

    return os;
}

}
}