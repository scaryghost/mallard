#pragma once

#include <stdexcept>
#include <string>

namespace mallard {
namespace parser {

struct ParserException : public std::runtime_error {
    ParserException(const std::string& what);
};

struct NoMoreTokensException : public ParserException {
    NoMoreTokensException(const std::string& what);
};

}
}
