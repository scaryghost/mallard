#pragma once

#include "mallard/api/statement.hpp"

#include <istream>
#include <memory>

namespace mallard {
namespace parser {

struct StatementStream {
    virtual ~StatementStream();

    virtual std::shared_ptr<api::Statement> next() = 0;

    static std::shared_ptr<StatementStream> from_istream(std::istream& is);
};

}
}