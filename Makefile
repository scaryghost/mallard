VOLAUVENT_HOME=build-tools/Volauvent
CXXFLAGS := -std=c++14

.PHONY: all test clean

all: src/mallard/parser-all src/mallard/api-all 
clean: src/mallard/parser-clean src/mallard/api-clean test-clean
test: test-all

include $(VOLAUVENT_HOME)/global_config.mk

include src/rules.mk
include test/rules.mk
