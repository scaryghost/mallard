include $(VOLAUVENT_HOME)/application.mk
include $(VOLAUVENT_HOME)/static_library.mk

TEST_DEPS := $(src/mallard/api_APP_OUTPUT) $(src/mallard/parser_APP_OUTPUT)
TEST_INC_DIRS := $(src/mallard/api_INC_DIRS) $(src/mallard/parser_INC_DIRS)

$(eval $(call define_app_rules,test,mallard_test,$(TEST_DEPS),$(TEST_INC_DIRS)))
