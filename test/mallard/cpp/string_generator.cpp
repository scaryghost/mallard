#include "string_generator.hpp"

#include <cstdint>
#include <sstream>

using namespace std;

namespace mallard {
namespace generator {

PrintableStringGenerator::PrintableStringGenerator(uint32_t max_length) :
    rand(std::random_device{}()),
    distribution_lengths(0, max_length),
    distribution_printable_chars(32, 126)
{
    next();
}

const Strings& PrintableStringGenerator::get() const {
    return next_value;
}

bool PrintableStringGenerator::next() {
    stringstream buffer, buffer_raw;

    buffer << "\"";
    for(uint32_t i = 0; i < distribution_lengths(rand); i++) {
        uint32_t code = distribution_printable_chars(rand);

        buffer_raw << (uint8_t) code;
        switch(code) {
            case 34:
                buffer << "\\\"";
                break;
            case 39:
                buffer << "\\\'";
                break;
            case 92:
                buffer << "\\\\";
                break;
            default:
                buffer << (uint8_t) code;
                break;
        }
    }
    buffer << "\"";

    next_value = {
        .raw = buffer_raw.str(),
        .escaped = buffer.str()
    };
    return true;
}

}
}

Catch::Generators::GeneratorWrapper<mallard::generator::Strings> printable_string(uint32_t max_length ) {
    return Catch::Generators::GeneratorWrapper<mallard::generator::Strings>(
        new mallard::generator::PrintableStringGenerator(max_length)
    );
}