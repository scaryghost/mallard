#include "catch_amalgamated.hpp"

#include "string_generator.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/cpp/token_extension.hpp"
#include "mallard/parser/cpp/tokenizer.hpp"

#include <cstdint>
#include <cstring>
#include <iomanip>
#include <ios>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>

#include <iostream>

using namespace std;

namespace mallard {
namespace parser {

TEST_CASE("ints are tokenized", "[parser]") {
    auto value = GENERATE(take(25, random(0u, numeric_limits<uint32_t>::max())));

    stringstream content;
    content << dec << value;

    Token expected = {
        .value = content.str(),
        .type = Token::INT_LITERAL
    };

    CHECK(Tokenizer(content).next() == expected);
}

TEST_CASE("decimals are tokenized", "[parser]") {
    auto value = GENERATE(take(100, random(0.0f, 128.0f)));

    stringstream content;
    content << fixed << value;

    Token expected = {
        .value = content.str(),
        .type = Token::DOUBLE_LITERAL
    };

    CHECK(Tokenizer(content).next() == expected);
}

TEST_CASE("numerical literals negative cases", "[parser]") {
    auto value = GENERATE(
        as<string>{},
        "6.674.30",
        "1.f"
    );

    stringstream content;
    content << value;

    REQUIRE_THROWS_AS(Tokenizer(content).next(), ParserException);
}

TEST_CASE("strings are tokenized", "[parser]") {
    auto value = GENERATE(take(25, printable_string(128)));

    stringstream content;
    content << value.escaped;

    Token expected = {
        .value = value.raw,
        .type = Token::STRING_LITERAL
    };

    CHECK(Tokenizer(content).next() == expected);
}

TEST_CASE("identifiers are tokenized", "[parser]") {
    auto value = GENERATE(
        as<string>{},
        "_snake_case",
        "camelCase",
        "CONSTANT_CASE",
        "var_314159265358979",
        "__agent_007"
    );

    stringstream content;
    content << value;

    Token expected = {
        .value = value,
        .type = Token::IDENTIFIER
    };

    CHECK(Tokenizer(content).next() == expected);
}

TEST_CASE("symbols are tokenized", "[parser]") {
    auto value = GENERATE(
        as<string>{},
        "( )",
        "{ }",
        "[ ]",
        "? :",
        "/ /= % %= = ==",
        "+ ++ +=",
        "- -- -= ->",
        "* ** *=",
        "< << <=",
        "> >> >=",
        "~ ~=",
        "! !=",
        "^ ^^ ^=",
        "| || |=",
        "& && &=",
        ". .."
    );

    stringstream content, expected_stream;

    content << value;
    expected_stream << value;

    try {
        auto tokenizer = Tokenizer(content);

        while(true) {
            auto actual = tokenizer.next();
            string expected;

            getline(expected_stream, expected, ' ');
            
            CHECK(actual.value == expected);
            CHECK(actual.type == Token::SYMBOL);
        }
    } catch (const NoMoreTokensException& e) {
    }
}

TEST_CASE("correctly tokenize dot character", "[parser]") {
    auto value = GENERATE(values<tuple<string, vector<Token>>>({
        {
            "3..4",
            {
                {
                    .value = "3",
                    .type = Token::INT_LITERAL
                }, 
                {
                    .value = "..",
                    .type = Token::SYMBOL
                },
                {
                    .value = "4",
                    .type = Token::INT_LITERAL
                }
            }
        },
        {
            "3.14159",
            {
                {
                    .value = "3.14159",
                    .type = Token::DOUBLE_LITERAL
                }
                
            }
        },
        {
            "2.71828f",
            {
                {
                    .value = "2.71828f",
                    .type = Token::FLOAT_LITERAL
                }
            }
        },
        {
            "7.minutes",
            {
                {
                    .value = "7",
                    .type = Token::INT_LITERAL
                },
                {
                    .value = ".",
                    .type = Token::SYMBOL
                },
                {
                    .value = "minutes",
                    .type = Token::IDENTIFIER
                }
            }
        }
    }));

    stringstream content;
    content << get<0>(value);

    auto expected = get<1>(value).begin();
    try {
        auto tokenizer = Tokenizer(content);

        while(true) {
            auto actual = tokenizer.next();
            
            CHECK(actual == *expected);
            expected++;
        }
    } catch (const NoMoreTokensException& e) {
        CHECK(expected == get<1>(value).end());
    }
}

}
}
