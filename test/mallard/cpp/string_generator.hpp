#pragma once

#include "catch_amalgamated.hpp"

#include <cstdint>
#include <random>
#include <string>

namespace mallard {
namespace generator {

struct Strings {
    std::string raw;
    std::string escaped;
};

struct PrintableStringGenerator final : public Catch::Generators::IGenerator<Strings> {
    PrintableStringGenerator(std::uint32_t max_length);

    Strings const& get() const override;

    bool next() override;

private:
    std::minstd_rand rand;
    std::uniform_int_distribution<> distribution_lengths, distribution_printable_chars;
    Strings next_value;
};

}
}

Catch::Generators::GeneratorWrapper<mallard::generator::Strings> printable_string(std::uint32_t max_length );