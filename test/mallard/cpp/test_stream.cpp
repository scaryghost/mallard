#include "catch_amalgamated.hpp"

#include "mallard/api/statement_debug.hpp"
#include "mallard/api/statement_extension.hpp"
#include "mallard/parser/stream.hpp"

#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

TEST_CASE("int literal statements", "[parser]") {
    auto value = GENERATE(take(25, random(0u, numeric_limits<uint32_t>::max())));

    stringstream content;
    content << dec << value;

    auto stream = StatementStream::from_istream(content);

    auto expected = api::Statement {
        .literals = { content.str() },
        .productions = { },
        .id = api::Statement::INT_LITERAL
    };

    CHECK(*stream->next() == expected);
}

TEST_CASE("simple binary expression", "[parser]") {
    stringstream content;
    content << "x + y * z";

    auto stream = StatementStream::from_istream(content);

    auto x = make_shared<api::Statement>(
        api::Statement{
            .literals = { "x" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto y = make_shared<api::Statement>(
        api::Statement {
            .literals = { "y" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto z = make_shared<api::Statement>(
        api::Statement {
            .literals = { "z" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );

    auto y_mul_z = make_shared<api::Statement>(
        api::Statement {
            .literals = { "*" },
            .productions = { y, z },
            .id = api::Statement::BINARY_EXPRESSION
        }
    );

    auto expected = api::Statement {
        .literals = { "+" },
        .productions = { x, y_mul_z },
        .id = api::Statement::BINARY_EXPRESSION
    };

    CHECK(*stream->next() == expected);
}

TEST_CASE("operator priority", "[parser]") {
    stringstream content;
    content << "x + y * z / w";

    auto stream = StatementStream::from_istream(content);

    auto x = make_shared<api::Statement>(
        api::Statement{
            .literals = { "x" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto y = make_shared<api::Statement>(
        api::Statement {
            .literals = { "y" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto z = make_shared<api::Statement>(
        api::Statement {
            .literals = { "z" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto w = make_shared<api::Statement>(
        api::Statement {
            .literals = { "w" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );

    auto y_mul_z = make_shared<api::Statement>(
        api::Statement {
            .literals = { "*" },
            .productions = { y, z },
            .id = api::Statement::BINARY_EXPRESSION
        }
    );

    auto y_mul_z_div_w = make_shared<api::Statement>(
        api::Statement {
            .literals = { "/" },
            .productions = { y_mul_z, w },
            .id = api::Statement::BINARY_EXPRESSION
        }
    );

    auto expected = api::Statement {
        .literals = { "+" },
        .productions = { x, y_mul_z_div_w },
        .id = api::Statement::BINARY_EXPRESSION
    };

    CHECK(*stream->next() == expected);
}

TEST_CASE("assignment is lowest priority", "[parser]") {
    stringstream content;
    content << "val F = dp / dt";

    auto stream = StatementStream::from_istream(content);

    auto dp = make_shared<api::Statement>(
        api::Statement {
            .literals = { "dp" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );
    auto dt = make_shared<api::Statement>(
        api::Statement {
            .literals = { "dt" },
            .productions = { },
            .id = api::Statement::VARIABLE
        }
    );

    auto dp_div_dt = make_shared<api::Statement>(
        api::Statement {
            .literals = { "/" },
            .productions = { dp, dt },
            .id = api::Statement::BINARY_EXPRESSION
        }
    );

    auto expected = api::Statement {
        .literals = { "F" },
        .productions = { dp_div_dt },
        .id = api::Statement::ASSIGNMENT
    };

    CHECK(*stream->next() == expected);
}


}
}